<?php

class UnsafeOpensslAES
{
    const METHOD = 'aes-256-cbc';

    public static function encrypt($message, $key)
    {
        if (mb_strlen($key, '8bit') !== 32) {
            throw new Exception("Needs a 256-bit key!");
        }
        $ivsize = openssl_cipher_iv_length(self::METHOD);
        $iv = openssl_random_pseudo_bytes($ivsize);

        $ciphertext = openssl_encrypt(
                $message,
                self::METHOD,
                $key,
                OPENSSL_RAW_DATA,
                $iv
                );

        return $iv . $ciphertext;
    }

    public static function decrypt($message, $key)
    {
        if (mb_strlen($key, '8bit') !== 32) {
            throw new Exception("Needs a 256-bit key!");
        }
        $ivsize = openssl_cipher_iv_length(self::METHOD);
        $iv = mb_substr($message, 0, $ivsize, '8bit');
        $ciphertext = mb_substr($message, $ivsize, null, '8bit');

        return openssl_decrypt(
                $ciphertext,
                self::METHOD,
                $key,
                OPENSSL_RAW_DATA,
                $iv
                );
    }
}

$key = 'qwertyuiopasdfghjklzxcvbnmqwerty';
$aes= new UnsafeOpensslAES();

$nka = 'N1409442_20170101';
echo '<h2>Szyfruje string: '.$nka.'</h2>';
echo '<h3>'.base64_encode($aes->encrypt($nka, $key)).'</h3>';

function kodujXX($key,$nka){

    $qEncoded = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_128, md5($key), $nka,MCRYPT_MODE_CBC, md5(md5($key))));
    return $qEncoded;
}

function pkcs5_pad ($text, $blocksize) {
    $pad = $blocksize - (strlen($text) % $blocksize);
    return $text . str_repeat(chr($pad), $pad);
}

function encryptOnly($plaintext, $key)
{
    $size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
    $iv = mcrypt_create_iv($size, MCRYPT_DEV_URANDOM);
    $ciphertext = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key, $plaintext, MCRYPT_MODE_CBC, $iv);
    return $iv.$ciphertext;
}


print kodujXX($key, $nka);
print '<br>';
print base64_encode(encryptOnly(pkcs5_pad($nka, 16),$key));

?>