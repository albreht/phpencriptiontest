<?php
$methods = openssl_get_cipher_methods();

//var_dump($methods);

$textToEncrypt = "test";
$secretKey = "klucz";

echo '<pre>';
foreach ($methods as $method) {
    //$encrypted = openssl_encrypt($textToEncrypt, $method, $secretKey);
    //$decrypted = openssl_decrypt($encrypted, $method, $secretKey);
  //  echo $method . ': ' . $encrypted . ' ; ' . $decrypted . "\n";
}
echo '</pre>';


class Foo {
    protected $mcrypt_cipher = MCRYPT_RIJNDAEL_128;
    protected $mcrypt_mode = MCRYPT_MODE_CBC;

    public function decrypt($key, $iv, $encrypted)
    {
        $iv_utf = mb_convert_encoding($iv, 'UTF-8');
        return mcrypt_decrypt($this->mcrypt_cipher, $key, $encrypted, $this->mcrypt_mode, $iv_utf);
    }
    public function encrypt($key,$iv,$data){

        $iv_utf = mb_convert_encoding($iv, 'UTF-8');
        $data_utf = mb_convert_encoding($data, 'UTF-8');
        $key_utf = mb_convert_encoding($key, 'UTF-8');
        echo $data_utf;
        return mcrypt_encrypt($this->mcrypt_cipher, $key_utf, $data_utf, $this->mcrypt_mode,$iv_utf);
    }
}
class UnsafeOpensslAES
{
    const METHOD = 'aes-256-cbc';

    public static function encrypt($message, $key)
    {
        if (mb_strlen($key, '8bit') !== 32) {
            throw new Exception("Needs a 256-bit key!");
        }
        $ivsize = openssl_cipher_iv_length(self::METHOD);
        $iv = openssl_random_pseudo_bytes($ivsize);

        $ciphertext = openssl_encrypt(
                $message,
                self::METHOD,
                $key,
                OPENSSL_RAW_DATA,
                $iv
                );

        return $iv . $ciphertext;
    }

    public static function decrypt($message, $key)
    {
        if (mb_strlen($key, '8bit') !== 32) {
            throw new Exception("Needs a 256-bit key!");
        }
        $ivsize = openssl_cipher_iv_length(self::METHOD);
        $iv = mb_substr($message, 0, $ivsize, '8bit');
        $ciphertext = mb_substr($message, $ivsize, null, '8bit');

        return openssl_decrypt(
                $ciphertext,
                self::METHOD,
                $key,
                OPENSSL_RAW_DATA,
                $iv
                );
    }
}


$encrypted = "UmzUCnAzThH0nMkIuMisqg==";
$key = "qwertyuiopasdfghjklzxcvbnmqwerty";
$iv = "1234567890123456";
$data="test";

$foo = new Foo;

echo $data;
echo '</br>';
 $encrypted=$foo->encrypt($key, $iv, $data);
echo $encrypted;
echo '</br>';
echo base64_encode($encrypted);
echo '</br>';
echo $foo->decrypt($key, $iv, $encrypted);

echo '<br>';
$unsafessl =  new UnsafeOpensslAES();
echo $unsafessl->encrypt($data, $key);
echo '<br>';
echo base64_encode($unsafessl->encrypt($data, $key));
?>